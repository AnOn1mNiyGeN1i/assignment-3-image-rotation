#include "image_handler.h"
#include "image_rotate.h"
#include <stddef.h>
#include <stdint.h>
IMAGE_TRANSFORMER_IMAGE_ROTATE_H

struct image rotate_90(struct image src) {
    struct image rotated = image_init(src.height, src.width);

    for (uint64_t i = 0; i < rotated.height; ++i) {
        for (uint64_t j = 0; j < rotated.width; ++j) {
            rotated.data[i * rotated.width + j] = src.data[(j + 1) * src.width - 1 - i];
        }
    }

    return rotated;
}


struct image rotate(struct image *src, int32_t angle) {
    struct image result = image_init(src->width, src->height);
    for (size_t i = 0; i < src->width * src->height; ++i) {
        result.data[i] = src->data[i];
    }

    if (angle < 0) {
        angle += 360;
    }

    int32_t rotations = angle / 90;

    for (long i = 0; i < rotations; ++i) {
        //printf("%d", 4);
        struct image temp = rotate_90(result);
        //printf("%d", 5);
        image_destroy(&result);
        result = temp;
    }

    return result;
}

