#include "bmp_format.h"
#include <stddef.h>
#include  <stdint.h>
#include <stdio.h>

#define BMP_SIGN 0x4D42
#define BIT_COUNT 24
#define BF_RESERVED 0
#define BI_SIZE 40
#define BI_PELS_PER_METER 0
#define BI_CLR_USED 0
#define BI_CLR_IMPORTANT 0
IMAGE_TRANSFORMER_BMP_FORMAT_H
#pragma pack(push, 1)


struct bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)


/*
 * padding is uint16, because uint32 cast to long
 * is not implementation defined, and it is larger than uint 8)
 */
uint16_t get_padding(const uint32_t width) {
    return (4 - (width * sizeof(struct pixel) % 4)) % 4;
}

enum read_status from_bmp(FILE *in, struct image *img) {
    struct bmp_header header = {0};

    if (fread(&header, sizeof(struct bmp_header), 1, in) != 1) {
        return READ_INVALID_HEADER;
    }

    if (header.bfType != BMP_SIGN) {
        return READ_INVALID_SIGNATURE;
    }

    if (header.biBitCount != BIT_COUNT) {
        return READ_INVALID_BITS;
    }


    if (header.biHeight > 0) {
        *img = image_init(header.biWidth, header.biHeight);
    } else {
        *img = image_init(header.biWidth, -1 * header.biHeight);
    }
    uint32_t h_width = header.biWidth;
    //uint32_t h_height = header.biHeight;

    uint16_t padding = get_padding(h_width);

    for (size_t i = 0; i < img->height; i++) {
        if (fread(img->data + i * h_width, sizeof(struct pixel), h_width, in) != h_width) {
            image_destroy(img);
            return READ_INVALID_BITS;
        }
        if (fseek(in, padding, SEEK_CUR) != 0) {
            image_destroy(img);
            return READ_INVALID_BITS;
        }
    }


    return READ_OK;
}


enum write_status to_bmp(FILE *out, const struct image *image) {
    uint16_t padding = get_padding(image->width);

    struct bmp_header header;

    header.bfType = BMP_SIGN;
    header.bOffBits = sizeof(struct bmp_header);
    header.biWidth = image->width;
    header.biHeight = image->height;
    header.biSizeImage = (sizeof(struct pixel) * header.biWidth + padding) * header.biHeight;
    header.bfileSize = header.biSizeImage + header.bOffBits;
    header.bfReserved = BF_RESERVED;
    header.biSize = BI_SIZE;
    header.biPlanes = 1;
    header.biBitCount = BIT_COUNT;
    header.biCompression = 0;
    header.biXPelsPerMeter = BI_PELS_PER_METER;
    header.biYPelsPerMeter = BI_PELS_PER_METER;
    header.biClrUsed = BI_CLR_USED;
    header.biClrImportant = BI_CLR_IMPORTANT;

    if (!fwrite(&header, sizeof(struct bmp_header), 1, out)) {
        return WRITE_ERROR;
    }

    for (size_t i = 0; i < image->height; i++) {
        if (fwrite(image->data + i * image->width, sizeof(struct pixel), image->width, out) != image->width) {
            return WRITE_ERROR;
        }

        for (uint16_t pad = 0; pad < padding; pad++) {
            if (fputc(0, out) == EOF) {
                return WRITE_ERROR;
            }
        }
    }


    return WRITE_OK;
}




