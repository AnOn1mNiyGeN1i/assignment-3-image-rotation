#include "bmp_format.h"
#include "image_rotate.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>


int main(int argc, char **argv) {
    char *end;
    if (argc != 4) {
        fprintf(stderr, "Args: <source-image> <transformed-image> <angle>\n");
        return -1;
    }

    long angle_long = strtol(argv[3], &end, 10);
    int32_t angle = (int32_t) angle_long;
    if (angle != 0 && angle != 90 && angle != -90 && angle != 180 && angle != -180 && angle != 270 && angle != -270) {
        fprintf(stderr, "Invalid angle. Must be one of: 0, 90, -90, 180, -180, 270, -270\n");
        return -1;
    }

    FILE *in = fopen(argv[1], "rb");
    if (!in) {
        perror("Error opening source image");
        return -1;
    }

    FILE *out = fopen(argv[2], "wb");
    if (!out) {
        perror("Error opening destination image");
        fclose(in);
        return -1;
    }

    struct image src = {0};
    if (from_bmp(in, &src) != READ_OK) {
        fprintf(stderr, "Error reading source image\n");
        fclose(in);
        fclose(out);
        return -1;
    }

    struct image result = rotate(&src, angle);

    if (to_bmp(out, &result) != WRITE_OK) {
        fprintf(stderr, "Error writing transformed image\n");
    }
    fclose(in);
    fclose(out);
    image_destroy(&src);
    image_destroy(&result);

    return 0;

}
