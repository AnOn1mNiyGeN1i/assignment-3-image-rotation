#include "image_handler.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

IMAGE_TRANSFORMER_IMAGE_HANDLER_H
/*
 * image initializer
 */
struct image image_init(const uint64_t width, const uint64_t height) {
    struct image img;
    img.width = width;
    img.height = height;
    img.data = (struct pixel *) malloc(sizeof(struct pixel) * height * width);
    if (img.data == NULL) {
        fprintf(stderr, "Memory allocation error\n");
        exit(1);
    }
    return img;
}

/*
 * image destructor
 */
void image_destroy(struct image *img) {
    if (img->data != NULL) {
        free(img->data);
    }
}


