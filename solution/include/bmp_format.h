#include "image_handler.h"
#include <stdint.h>
#include <stdio.h>
#ifndef IMAGE_TRANSFORMER_BMP_FORMAT_H
#define IMAGE_TRANSFORMER_BMP_FORMAT_H

enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
};

enum read_status from_bmp(FILE *in, struct image *img);

enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR
};

enum write_status to_bmp(FILE *out, const struct image *image);
#endif //IMAGE_TRANSFORMER_IMAGE_HANDLER_H
