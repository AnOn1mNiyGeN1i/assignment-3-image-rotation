#include <inttypes.h>
#ifndef IMAGE_TRANSFORMER_IMAGE_HANDLER_H
#define IMAGE_TRANSFORMER_IMAGE_HANDLER_H



struct pixel {
    uint8_t b, g, r;
};

struct image {
    uint64_t width, height;
    struct pixel *data;
};


struct image image_init(uint64_t width, uint64_t height);

void image_destroy(struct image *img);
#endif //IMAGE_TRANSFORMER_IMAGE_HANDLER_H

