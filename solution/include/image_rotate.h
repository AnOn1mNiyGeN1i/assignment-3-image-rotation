#include <stdint.h>

#ifndef IMAGE_TRANSFORMER_IMAGE_ROTATE_H
#define IMAGE_TRANSFORMER_IMAGE_ROTATE_H


struct image rotate_90(struct image src);


struct image rotate(struct image *src, int32_t angle);

#endif //IMAGE_TRANSFORMER_IMAGE_ROTATE_H
